package com.jain.jinwani;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.jain.jinwani.databinding.FragmentAppDeveloperBinding;

public class AppDeveloperFragment extends Fragment {
    FragmentAppDeveloperBinding dataBiding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_app_developer, null, false);
        dataBiding.setHandlers(new MyClickHandlers());
        return dataBiding.getRoot();
    }

    public class MyClickHandlers {
        public void onWhatsappClick(View view) {
            openWhatsApp();
        }

        public void onContactClick(View view) {
            createCall();
        }

        public void onMailClick(View view) {
            openMail();
        }
    }

    private void openMail() {
        String[] TO = {dataBiding.tvEmail.getText().toString()};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        /*  emailIntent.putExtra(Intent.EXTRA_CC, CC);*/
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Enter message here...");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void createCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + dataBiding.tvNumber.getText().toString()));
        startActivity(intent);
    }


    public void openWhatsApp() {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + dataBiding.tvWhatsapp.getText().toString() + "?body=" + ""));
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "it may be you dont have whats app", Toast.LENGTH_LONG).show();

        }
    }
}

